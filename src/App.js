import "./App.css";
import React, { useEffect, useState } from "react";

function App() {
  const [state, setState] = useState(0);
  const [size, setSize] = useState(0);

  useEffect(() => {
    setTimeout(() => setState(Math.floor(Math.random() * 1000000000)), 900);
  }, [state]);

  useEffect(() => {
    window.addEventListener("resize", resize());
  }, []);

  const resize = () => {
    setSize(window.innerWidth <= 760);
  };
  return (
    <div>
      <div
        className='button-primary'
        onClick={() => {
          setState(Math.floor(Math.random() * 1000000000));
          window.open(
            "https://www.linkedin.com/in/wahyu-fatur-rizky/",
            "_blank",
            false
          );
          window.open("https://dribbble.com/wahyu_faturrizky", "_blank", false);
          window.open("https://github.com/wahyufaturrizky", "_blank", false);
          window.open("https://gitlab.com/wahyufaturrizky", "_blank", false);
        }}
      >
        Clicked Me !
      </div>
      <div className='content'>
        <h1>{state}</h1>
      </div>
    </div>
  );
}

export default App;
